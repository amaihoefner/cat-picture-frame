#!/usr/bin/python
# -*- coding:utf-8 -*-
import argparse
import os
import random
import logging
from PIL import Image
import shutil
import requests
# Replace "epd5in65f" with your display and adjust epaper_size to the resolution of your display
# A list of all available displays can be found here: https://github.com/waveshare/e-Paper/tree/master/RaspberryPi_JetsonNano/python/lib/waveshare_epd
from waveshare_epd import epd5in65f as epaper_display
epaper_size = (600, 448)

parser = argparse.ArgumentParser()

image_source_group = parser.add_mutually_exclusive_group()

image_source_group.add_argument("--image_url", help="remote image url", default="https://cataas.com/cat", type=str)
image_source_group.add_argument("--image", help="image file", type=str)
image_source_group.add_argument("--image_dir", help="local image directory", type=str)

args = parser.parse_args()


logging.basicConfig(level=logging.DEBUG)

def get_image(url, output_file):
    response = requests.get(url, stream=True)
    with open(output_file, 'wb') as out_file:
        shutil.copyfileobj(response.raw, out_file)
    del response


def display(image):
    """
    Display given image (automatically resized to fit) on e-paper display.

    Parameters:
        image (str): Image file path
    """
    image = Image.open(image)
    image = image.resize(epaper_size)

    try:
        logging.info("picture-frame")

        epd = epaper_display.EPD()
        logging.info("init and Clear")
        epd.init()

        epd.display(epd.getbuffer(image))

        logging.info("Goto Sleep...")
        epd.sleep()

    except IOError as e:
        logging.info(e)

    except KeyboardInterrupt:
        logging.info("ctrl + c:")
        epaper_display.epdconfig.module_exit()
        exit()

if __name__ == "__main__":

    image_file = ""

    if args.image:
        image_file = args.image
    elif args.image_dir:
        image_file = args.image_dir + "/" + random.choice(os.listdir(args.image_dir))
    else:
        image_file = "download.jpg"

        get_image(args.image_url, image_file)

    display(image_file)
