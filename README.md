![Assembled picture frame](./picture-frame.jpg)

Use a RaspberryPi Zero W to fetch a random cat picture from either https://cataas.com/ or from disk and display it on a 7 color 5.65 e-Paper display. This was a birthday present. It can easily be adjusted to display other images or to use a different display by editing "display_image.py".

# Setup
You can use [Crontab Guru](https://crontab.guru) to help with the cronjob values (also see the [examples](https://crontab.guru/examples.html)).

## With Ansible
1. cd into "ansible"
2. Adjust ansible_host, ansible_user and ansible_password in "hosts" (optionally add more hosts)
3. Run `ansible-playbook main.yml -i hosts`

The ansible playbook creates a cronjob. You can edit when the cronjob is run (i.e. when a new image is displayed) by editing the cronjob task in main.yml (or after the fact by sshing onto the pi and running `crontab -e`).

## Manual
* Follow the guide from the [documentation](https://www.waveshare.com/wiki/5.65inch_e-Paper_Module_(F)#Users_Guides_of_Raspberry_Pi).
* cd into "e-Paper/RaspberryPi_JetsonNano/python" and run `python3 setup.py install`
* copy "display_image.py" onto the pi
* Optional: Setup a cronjob to periodically run `python3 display_image.py`

# Usage
1. Without arguments (`python3 display_image.py`): Fetch image from default url (https://cataas.com/cats)
2. With --image_url (`python3 display_image.py --image_url "https://cataas.com/cats"`): Fetch image from provided url 
3. With --image (`python3 display_image.py --image "cute_cat.jpg"`): Display provided image
4. With --image_dir (`python3 display_image.py --image_dir "./images"`): Display random image out of provided directory
